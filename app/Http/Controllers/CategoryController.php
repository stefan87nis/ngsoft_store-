<?php

namespace App\Http\Controllers;


use App\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    
    
    // prikazivanje tabele kategorije
    public function index(){
        
        
        $categories = Category::all();
        
        return view('categories.index', compact('categories'));
    }
    
    //prikazivanje forme za ubacivanje kategorija
    public function create(){
        
        $categories = Category::all();

        
        return view('categories.addCategory', compact('categories'));
    }
    
    //metoda pomocu koje ubacujemo kategorije
    public function store(){
        
        
        $data = request()->validate([
            'title' => 'required|min:3'
            ]);
        
        Category::create($data);
        return redirect('categories.index')->with('message', 'Category is added successfuly');
    }
    
}
