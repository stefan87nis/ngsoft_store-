<?php

namespace App\Http\Controllers;


use App\Product;
use App\Category;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    
    public function index(){
        
     $products = Product::paginate(5);
        
     return view('products.index', compact('products', 'categories'));
     
     
    }
    
    public function create(){
        
        $categories = Category::all();
        
        return view('products.addProducts', compact('categories', 'categories'));
        
    }
    
    public function store(){
        
        $data = request()->validate([
            'name' => 'required|min:3',
            'price' => 'required',
            'description' => 'required',
            'category_id' => 'required',
            'image' => 'required|image'
            ]);
        
        
        
        
        Product::create($data);
        
        return redirect('products.index')->with('message', 'Item is added successfuly');
        
    }


    public function show($product){
        
        $product = Product::find($product);
        
        return view('products.product-details', compact('product'));
    }
}