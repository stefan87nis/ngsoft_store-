
<ul class="nav justify-content-end">
  <li class="nav-item">
    <a class="nav-link active" href="{{ url('/') }}">Pocetna</a>
  </li>  
  <li class="nav-item">
    <a class="nav-link active" href="{{ url('categories.index') }}">Kategorije</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="{{ url('products.index') }}">Proizvodi</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="#">Link</a>
  </li>
</ul>