<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    
    <script src="/js/bootstrap.min.js"></script>
    
    
    <title>Hello, world!</title>
  </head>
  <body>

      <div class="container">
          <div class="content">
              @include('inc/navbar')
              
              @if(session()->has('message'))
                <div class="alert alert-success" role="alert">
                    {{ session()->get('message') }}
                </div>
              @endif
              
              @yield('inc/content')
              
              
          </div>
      </div>
  </body>
</html>