@extends('inc/layout')

@section('inc/content')

              <h1 class="my-5" align="center">Kategorije</h1>
              
              
              
              
              
<p><a href="categories.addCategory">Dodaj kategoriju</a></p>
<table class="table">
  <thead>
    <tr>
      <th scope="col">Id</th>
      <th scope="col">Title</th>
    </tr>
  </thead>
  <tbody>
      @foreach($categories as $category)
    <tr>
      <th scope="row">{{ $category->id }}</th>
      <td>{{ $category->title }}</td>
    </tr>
    @endforeach
  </tbody>
</table>
              
@endsection