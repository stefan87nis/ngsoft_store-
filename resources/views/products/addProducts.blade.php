@extends('inc/layout')

@section('inc/content')

              <h1 class="my-5" align="center">Dodaj kategoriju</h1>
              

<form action="{{ route('products.store') }}" method="post" enctype="multipart/form-data">

  <div class="form-row">

    <div class="form-group col-md-6">
      <label for="inputTitle">Name</label>
      <input type="text" name="name" class="form-control" id="inputTitle" placeholder="Product title">
      
     {{ $errors->first('name') }}
     
    </div>

    <div class="form-group col-md-6">
      <label for="inputCategory">Category</label>
      
      <select name="category_id" name="title" id="inputCategory" class="form-control">
        @foreach( $categories as $category )
          <option value="{{ $category->id }}">
            {{ $category->title }}
          </option>
          @endforeach
      </select>
      
    </div>

  </div>

  <div class="form-row">

    <div class="form-group col-md-6">
      <label for="image">Image</label>
      <input type="file" id="image" name="image">
      {{ $errors->first('image') }}
      
    </div>

    <div class="form-group col-md-6">
      <label for="inputPrice">Price</label>
      <input type="number" name="price" id="inputPrice" step="0.01" class="form-control" placeholder="Product price" />
      
      {{ $errors->first('price') }}
      
    </div>

  </div>

  <div class="form-row">

    <div class="form-group col-md-12">
      <label for="inputDescription">Description</label>
      <textarea name="description" class="form-control" id="inputDescription" rows="3" placeholder="Product description..."></textarea>
      
      {{ $errors->first('description') }}
      
    </div>
      
  </div>
    
  <div class="d-flex justify-content-end">
    <button type="submit" class="btn btn-outline-primary">Dodaj proizvod</button>
  </div>
    @csrf
</form>
    
              

              
@endsection