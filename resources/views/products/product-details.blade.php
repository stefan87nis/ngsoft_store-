@extends('inc/layout')

@section('inc/content')

<h1 class="my-5">Naslov</h1>

<div class="row">

  <div class="col-md-5">{{ $product->image }}</div>

  <div class="col-md-7">
    <h3 class="mb-5">{{ $product->description }}</h3>
    <p class="mb-5">
      {{ $product->description }}
    </p>

    <h5 class="mt-5">{{ $product->price }}</h5>

  </div>
  
</div>


@endsection